//
//  main.m
//  TwoFloats
//
//  Created by Denis Vytryshko on 17.10.15.
//  Copyright © 2015 Denis Vytryshko. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        
        float  valFirst = 42.0f;
        float  pi       = 3.14f;
        double sum      = valFirst + pi;
        
        NSLog(@"The sum of two values %f  and  %f(float) = %f!", valFirst, pi, sum);
        NSLog(@"The sum of two values %f  and  %f (integer) = %0.f!", valFirst, pi,sum);
    }
    return 0;
}
